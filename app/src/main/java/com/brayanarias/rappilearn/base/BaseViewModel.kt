package com.brayanarias.rappilearn.base

import android.arch.lifecycle.ViewModel
import com.brayanarias.rappilearn.injection.component.DaggerViewModelInjector
import com.brayanarias.rappilearn.injection.component.ViewModelInjector
import com.brayanarias.rappilearn.injection.module.NetworkModule
import com.brayanarias.rappilearn.ui.post.PostListViewModel

open class BaseViewModel: ViewModel() {

    private val injector: ViewModelInjector = DaggerViewModelInjector
            .builder()
            .networkModule(NetworkModule)
            .build()


    init {
        inject()
    }

    private fun inject() {
        when (this) {
            is PostListViewModel -> injector.inject(this)
        }
    }

}