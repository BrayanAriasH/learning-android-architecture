package com.brayanarias.rappilearn.util

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.databinding.BindingAdapter
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.TextView
import com.brayanarias.rappilearn.util.extension.getParentActivity

@BindingAdapter("mutableVisibility")
fun setMutableVisibility(view: View, mutableVisibility: MutableLiveData<Int>) {
    val parentActivity: AppCompatActivity? = view.getParentActivity()
    if (parentActivity != null) {
        mutableVisibility.observe(parentActivity, Observer { value ->
            view.visibility = value ?: View.VISIBLE
        })
    }
}

@BindingAdapter("mutableText")
fun setMutableText(view: TextView, text: MutableLiveData<String>){
    val parentActivity = view.getParentActivity()
    if (parentActivity != null){
        text.observe(parentActivity, Observer {
            value -> view.text = value ?: ""
        })
    }
}

@BindingAdapter("adapter")
fun setAdapter(view: RecyclerView, adapter: RecyclerView.Adapter<*>){
    val parentActivity = view.getParentActivity()
    if (parentActivity != null){
        view.adapter = adapter
    }
}

@BindingAdapter("mutableTitle")
fun setMutableTitle(view: Toolbar, text: MutableLiveData<Int>){
    val parentActivity = view.getParentActivity()
    if (parentActivity != null){
        view.title = view.context?.getString(text.value!!)
    }
}