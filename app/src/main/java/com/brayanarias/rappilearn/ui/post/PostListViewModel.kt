package com.brayanarias.rappilearn.ui.post

import android.arch.lifecycle.MutableLiveData
import android.view.View
import com.brayanarias.rappilearn.R
import com.brayanarias.rappilearn.base.BaseViewModel
import com.brayanarias.rappilearn.model.Post
import com.brayanarias.rappilearn.model.PostDao
import com.brayanarias.rappilearn.network.PostApi
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class PostListViewModel(private val postDao: PostDao) : BaseViewModel() {

    @Inject
    lateinit var postApi: PostApi
    private lateinit var subscription: Disposable
    val dialogVisibility: MutableLiveData<Int> = MutableLiveData()
    val errorMessage:MutableLiveData<Int> = MutableLiveData()
    val errorClickListener = View.OnClickListener { loadPostOnline() }
    val postListAdapter = PostListAdapter()
    val postListTitle = MutableLiveData<Int>()

    init {
        postListTitle.value = R.string.txt_post_list_title
        loadPostOnline()
    }

    private fun loadPostOffline(){
        subscription = Observable.fromCallable { postDao.all }
                .concatMap {
                    if(it.isEmpty()){
                        postApi.getPost().concatMap {
                            apiPostList ->
                            postDao.insertAll(*apiPostList.toTypedArray())
                            Observable.just(apiPostList)
                        }
                    } else {
                        Observable.just(it)
                    }
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { onRetrievePostListStart() }
                .doOnTerminate { onRetrievePostListFinish() }
                .subscribe(
                        { result -> onRetrievePostListSuccess(result) },
                        { onRetrievePostListError() }
                )
    }
    private fun loadPostOnline() {
        subscription = postApi.getPost()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { onRetrievePostListStart() }
                .doOnTerminate { onRetrievePostListFinish() }
                .subscribe(
                        { result ->
                            postDao.deleteAll()
                            postDao.insertAll(*result.toTypedArray())
                            onRetrievePostListSuccess(result) },
                        { loadPostOffline() }
                )

    }

    private fun onRetrievePostListStart() {
        dialogVisibility.value = View.VISIBLE
        errorMessage.value = null

    }

    private fun onRetrievePostListFinish() {
        dialogVisibility.value = View.GONE
    }

    private fun onRetrievePostListSuccess(postList: List<Post>) {
        postListAdapter.updatePostList(postList)

    }

    private fun onRetrievePostListError() {
        errorMessage.value = R.string.post_error
    }

    override fun onCleared() {
        super.onCleared()
        subscription.dispose()
    }
}