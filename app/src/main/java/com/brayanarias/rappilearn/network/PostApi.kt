package com.brayanarias.rappilearn.network

import com.brayanarias.rappilearn.model.Post
import io.reactivex.Observable
import retrofit2.http.GET

interface PostApi {
    @GET("/posts")
    fun getPost(): Observable<List<Post>>
}