package com.brayanarias.rappilearn.model

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query

@Dao
interface PostDao {

    @get:Query("Select * From Post")
    val all: List<Post>

    @Insert
    fun insertAll(vararg post: Post)

    @Query("Delete from post")
    fun deleteAll()
}