package com.brayanarias.rappilearn.model.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.brayanarias.rappilearn.model.Post
import com.brayanarias.rappilearn.model.PostDao

@Database(entities = arrayOf(Post::class), version = 1)
abstract class AppDatabase : RoomDatabase(){
    abstract fun postDao(): PostDao
}